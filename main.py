import numpy as np

class BOARD:
    def __init__(self, height, width, preset = "presets/NONE"):
        if preset == "presets/NONE":
            self.field = np.zeros((height,width))
            self.size = self.field.shape
        elif preset == "presets/RANDOM":
            self.field = np.zeros((height,width))
            self.size = self.field.shape
            self.gen_rand_field()
        else:
            with open(preset,'r', encoding = 'utf-8') as f:
                height = int(f.read(2))
                f.read(1)
                width = int(f.read(2))
                self.field = np.zeros((height,width))   
                self.size = self.field.shape 
                for i in range(self.size[0]):
                    for j in range(self.size[1]):
                        temp = f.read(1)
                        while (temp != "1" and temp != "0"):
                            temp = f.read(1)
                        self.field[i][j] = int(temp)
            
    def gen_rand_field(self):
        import random as rn
        for i in range(self.size[0]):
            for j in range(self.size[1]):
                self.field[i][j] = rn.randint(0,1)
    
    def print_field(self, wait = 0):
        print("+", end="")
        for i in range(self.size[1]): print("—", end="")
        print("+")
        for i in range(self.size[0]):
            print("|", end="")
            for j in range(self.size[1]):
                if (self.field[i][j] == 1):
                    print("#", end="")
                elif (self.field[i][j] == 0): 
                    print(" ", end="")
                else:
                    print("?", end="")
            print("|")
            time.sleep(wait)
        print("+", end="")
        for i in range(self.size[1]): print("—", end="")
        print("+")

    def count_alive_neighbours(self, y, x):
        res = 0
        neighbours = [(1,0), (1,-1), (0,-1), (-1,-1), (-1,0), (-1,1), (0,1), (1,1)]
        for each in neighbours:
            temp = self.field[(y + each[0]) % self.size[0]][(x + each[1]) % self.size[1]]
            if temp == 1 or temp == 0:
                res += temp
        return res

    def update_cell(self, y, x):
        all_around = self.count_alive_neighbours(y, x)
        if ((self.field[y][x] == 0) and (all_around == 3)):
            return 1
        elif ((self.field[y][x] == 1) and (all_around != 3) and (all_around != 2)):
            return 0
        else:
            return self.field[y][x]
    
    def update_board(self):
        new_board = BOARD(self.size[0], self.size[1])
        for i in range(self.size[0]):
            for j in range(self.size[1]):
                new_board.field[i][j] = self.update_cell(i, j)
        return new_board               

def preset_choice(wait):    
    presets = ["Virus", "Symmetry", "Glider guns", "Stable pulsars", "Euscadron", "Launch this!", "Random"]
    preset_filenames = ["preset_1_SARS.txt", "preset_2_symmetry.txt", "preset_3_glider_guns.txt", "preset_4_stable_pulsars.txt","preset_5_euscadron.txt","launch_screen.txt", "RANDOM"]
    
    print("\033[1;32;40m\n")
    width = launch_screen_header(wait)

    while (1):
        print("CHOOSE YOUR PRESET".center(width))
        time.sleep(wait)
        for i in range(len(presets)):
            to_print = str(i+1) + ". " + presets[i]
            print(to_print.center(width))
            time.sleep(wait)
        choice = int(input())
        if choice < 1 or choice > len(presets):
            to_print = "There is no preset with number " + str(choice) + ", try again!"
            print(to_print.center(width))
            time.sleep(wait*10)
            os.system('cls' if os.name == 'nt' else 'clear')
            launch_screen_header(0)
        else:
            filename = "presets/"+preset_filenames[choice - 1]
            os.system('cls' if os.name == 'nt' else 'clear')
            launch_screen_header(0)
            to_print = 'Okay, "' + str(presets[choice-1]) + '" it is!'
            print("\n",to_print.center(width))
            time.sleep(wait*15)
            return BOARD(25, 80,filename)

def speed_choice(wait):
    os.system('cls' if os.name == 'nt' else 'clear')
    speed_opt = ["Very fast", "Fast", "Medium", "Slow", "Very slow"]
    width = launch_screen_header(0)
    wait = 0.1
    while (1):
        print("CHOOSE YOUR SPEED".center(width))
        time.sleep(wait)
        for i in range(len(speed_opt)):
            to_print = str(i+1) + ". " + speed_opt[i]
            print(to_print.center(width))
            time.sleep(wait)
        choice = int(input())
        if choice < 1 or choice > len(speed_opt):
            to_print = "There is no speed option with number " + str(choice) + ", try again!"
            print(to_print.center(width))
            time.sleep(wait*3)
        else:
            return choice * 0.25

def launch_screen_header(wait):
    board = BOARD(25, 80,"presets/launch_screen.txt")
    os.system('cls' if os.name == 'nt' else 'clear')
    width = board.size[1]+2
    print("WELCOME TO".center(width))
    time.sleep(wait)
    board.print_field(wait)
    time.sleep(wait)
    return width

import os
import time

if __name__ == '__main__':
    board = preset_choice(0.1)
    wait = speed_choice(0.1)
    while 1:
        os.system('cls' if os.name == 'nt' else 'clear')
        print()
        board.print_field()
        board = board.update_board()
        time.sleep(wait)
            