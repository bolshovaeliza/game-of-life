# Game of Life

This project is a Python implementation of a famous cellular automaton known as [**Conway's Game of Life**](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life).

![game_of_life](misc/game_of_life_header.png)

## Usage

This python programm allows you to run a simulation of a cellular automaton from the terminal. After starting the program you can choose a preset (such as a virus or euscadron) by inputting the corresponding integer into the terminal. Then you will be provided with the choice of simulation speed.

![speed](misc/speed_selection.png)